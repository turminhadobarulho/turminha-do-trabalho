//
//  MateriasTeste.swift
//  Turminha do Barulho
//
//  Created by Lucas Coiado Mota on 10/20/15.
//  Copyright © 2015 Lucas Coiado Mota. All rights reserved.
//

import UIKit

let materiasData = [
    Materia(name: "Biologia", color: UIColor(red: 12/255, green: 92/255, blue: 40/255, alpha: 1), icon: UIImage(named:"MedicinaIcon")),
    Materia(name: "Medicina Veterinária", color: UIColor(red: 89/255, green: 109/255, blue: 239/255, alpha: 1), icon: UIImage(named:"EngenhariaIcon")),
    Materia(name: "Gastronomia", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Direito", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Ciências Sociais", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Jornalismo", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Psicologia", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Medicina", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Química", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Relações Internacionais", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Ciências Biomédicas", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Cinema", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Economia", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Relações Públicas", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon")),
    Materia(name: "Publicidade e Propaganda", color: UIColor(red: 189/255, green: 50/255, blue: 239/255, alpha: 1), icon: UIImage(named:"HistoriaIcon"))
]
