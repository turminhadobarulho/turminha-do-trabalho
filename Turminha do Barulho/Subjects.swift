//
//  Subject.swift
//  Turminha do Barulho
//
//  Created by Lucas Coiado Mota on 11/10/15.
//  Copyright © 2015 Lucas Coiado Mota. All rights reserved.
//

import UIKit

struct Subjects{
    
    var subject : [String] = []
    
    init(subject: [String])
    {

        self.subject = subject
        
    }
    
}
